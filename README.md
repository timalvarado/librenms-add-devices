# LibreNMS Add Devices

## Description

This project is used to fetch devices from Netbox with specific roles and adds them to LibreNMS for monitoring.

## Deployment

### Prerequisites

* Netbox
* LibreNMS
* Python

1. Clone repo to scripts folder located in /opt/librenms/scripts and make sure librenms user has permission to repo
```
mkdir -p /opt/librenms/scripts/librenms-add-devices
git clone https://gitlab.com/timalvarado/librenms-add-devices.git /opt/librenms/scripts/librenms-add-devices
```

2. Setup cronjob as the librenms user with the following setup. *You can use any interval you want. I set it up for every hour

```
crontab -u librenms -e
```
```
0 * * * * /usr/bin/python3.6 /opt/librenms/scripts/librenms-add-devices/add-devices.py
```

3. Fill out .env file with all the information that is needed for your environment.


## To Do

* Add function to email if any errors occurs
* Be able to use snmp v1 and v3
* More error handling
* Auto generate .env file from secret store

## Authors

* **Tim Alvardo** - *Developer/Maintainer* - [Timalvarado](https://gitlab.com/timalvarado)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
