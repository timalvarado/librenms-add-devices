from dotenv import load_dotenv
import json
import os
import requests


def get_devices(netbox_url, netbox_headers, device_roles):

    '''
    This function gets all devices in each given role and appends it to a list and returns it.
    Default limit of 50 devices is set.
    If you expect to return more than 50 devices then read the netbox docs found here: https://netbox.readthedocs.io/en/stable/api/overview/#pagination
    '''
    devices = []
    for role in device_roles:
        device_url = netbox_url + '/' + '?role=' + role
        devices_response = requests.get(
            netbox_url,
            headers=netbox_headers
        )
        devices_json = devices_response.json()['results']
        for device in devices_json:
            # If the returned device has an IP address then append it to the list
            # I created the IP requirement so we are forced to document everything of device in Netbox
            if device['primary_ip']:
                name = device['name']
                devices.append(name)
    return devices


def add_devices(librenms_url, librenms_headers, device):

    try:
        # Try to add device with snmp info in config.php located at /opt/librenms
#        device_info = {
#            'hostname': device,
#            'version': 'v3',
#            'authlevel': os.getenv['AUTHLEVEL'],
#            'authname': os.getenv['AUTHNAME'],
#            'authpass': os.getenv['AUTHPASS'],
#            'authalgo': os.getenv['AUTHALGO'],
#            'cryptopass': os.getenv['CRYPTOPASS'],
#            'crytoalgo': os.getenv['CRYTOALGO']
#        }
        device_info = {
            'hostname': device,
            'version' : 'v1',
            'community': os.getenv('COMMUNITY')
        }
        requests.post(librenms_url, headers=librenms_headers, data=device_info, verify=False)
    except:
        # Add email alert
        # In email have alert that says had issue posting to librenms api
        pass


def main():

    # Load environment variables from .env
    load_dotenv()
    netbox_url = os.getenv('NETBOX_URL')
    librenms_url = os.getenv('LIBRENMS_URL')
    netbox_headers = {
        'Accept': 'application/json',
        'Authorization': 'Token ' + os.getenv('NETBOX_TOKEN'),
    }
    librenms_headers = {
        'X-Auth-Token': os.getenv('LIBRENMS_TOKEN')
    }

    # Device roles to search for in Netbox
    device_roles = [
        'access-point',
        'access-switch',
        'core-switch',
        'distribution-switch',
        'firwall',
        'management-switch',
        'router',
        'pdu',
        'ups'
    ]

    devices = get_devices(netbox_url, netbox_headers, device_roles)
    for device in devices:
        add_devices(librenms_url, librenms_headers, device)

if __name__ == "__main__":
    main()
