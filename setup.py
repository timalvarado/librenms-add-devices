from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='librenms-add-devices',
    version='1.0',
    description='Add devices to LibreNMS using Netbox and Librenms API',
    long_description=readme,
    author='Tim Alvarado',
    author_email='timmalvarado@gmail.com',
    url='https://gitlab.com/timalvarado/librenms-add-devices',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    keywords='netbox librenms Linux monitoring REST networking'
)
